// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package agent

import (
	"context"
	"net"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

type Config struct {
	Addr string
	Key  string
}

func MakeAgent(cfg *Config) *agent {
	return &agent{
		cfg: cfg,
	}
}

type agent struct {
	cfg *Config
}

func (a *agent) RunContext(ctx context.Context) error {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()
	l := &listener{
		cancel: cancel,
		ctx:    ctx,
		dial:   a.dial,
	}
	s := grpc.NewServer()
	reflection.Register(s)
	return s.Serve(l)
}

func (a *agent) dial() (net.Conn, error) {
	return net.Dial("tcp", a.cfg.Addr)
}
