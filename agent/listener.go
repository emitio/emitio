// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package agent

import (
	"context"
	"errors"
	"net"
	"time"
)

// listener acts as a net.Listener but is actually instantiating the connection, this
// allows the agent to dial the edge server while still having the agent act as a
// gRPC server.
type listener struct {
	cancel func()
	ctx    context.Context
	dial   func() (net.Conn, error)
}

// Accept waits for and returns the next connection to the listener.
func (l *listener) Accept() (net.Conn, error) {
	t := time.NewTimer(time.Duration(0))
	defer t.Stop()
	for {
		select {
		case <-l.ctx.Done():
			return nil, errors.New("listener is closed")
		case <-t.C:
			conn, err := l.dial()
			if err == nil {
				return conn, nil
			}
			// TODO better backoff handling.
			t.Reset(time.Second)
		}
	}
}

// Close closes the listener.
// Any blocked Accept operations will be unblocked and return errors.
func (l *listener) Close() error {
	l.cancel()
	return nil
}

// Addr returns the listener's network address.
func (l *listener) Addr() net.Addr {
	// TODO make custom net.Addr implementation
	return &net.IPAddr{}
}
