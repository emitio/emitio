// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package main

import (
	"context"
	"errors"
	"strings"

	"google.golang.org/grpc"

	"bitbucket.org/emitio/emitio/cmd"
	"bitbucket.org/emitio/emitio/signaler"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

func main() {
	viper.SetEnvPrefix("emitio")
	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer("-", "_"))

	var emitio = &cobra.Command{
		Use:   "emitio",
		Short: "emitio agent",
		PersistentPreRunE: func(cmd *cobra.Command, args []string) error {
			// user should be able to set variables via environment variables or command line flags.
			if err := viper.BindPFlags(cmd.PersistentFlags()); err != nil {
				return err
			}
			if err := viper.BindPFlags(cmd.Flags()); err != nil {
				return err
			}

			// set the global logging level.
			l, err := logrus.ParseLevel(viper.GetString("log-level"))
			if err != nil {
				return err
			}
			logrus.SetLevel(l)
			return nil
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			k := viper.GetString("key")
			if k == "" {
				return errors.New("emitio key must be provided")
			}
			s := viper.GetString("signaler")
			logrus.WithFields(logrus.Fields{
				"signaler": s,
			}).Info("dialing signaler")
			cc, err := grpc.Dial(s)
			if err != nil {
				return err
			}
			defer cc.Close()
			c := signaler.NewSignalerClient(cc)
			logrus.Info("requesting edge server")
			r, err := c.Signal(context.Background(), &signaler.SignalRequest{})
			if err != nil {
				return err
			}
			return nil
		},
	}
	emitio.PersistentFlags().String("log-level", "info", "The level of logging (panic|fatal|error|warn|info|debug)")
	emitio.AddCommand(cmd.Version())
	if err := emitio.Execute(); err != nil {
		logrus.WithError(err).Fatal("unable to execute command")
	}
}
